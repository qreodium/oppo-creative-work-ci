package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
)

type CalcStruct struct {
	First_number    float64 `json:"first_number"`
	Second_number 	float64 `json:"second_number"`
	Result 			float64 `json:"result"`
}

var calcHistory []CalcStruct

func getCalcHistoryHandler(w http.ResponseWriter, r *http.Request) {
	//Convert the "calcHistory" variable to json
	calcListBytes, err := json.Marshal(calcHistory)

	// If there is an error, print it to the console, and return a server
	// error response to the user
	if err != nil {
		fmt.Println(fmt.Errorf("Error: %v", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	// If all goes well, write the JSON list of calcHistory to the response
	w.Write(calcListBytes)
}

func executeCalcHandler(w http.ResponseWriter, r *http.Request) {
	// Create a new instance of CalcStruct
	calc := CalcStruct{}

	// We send all our data as HTML form data
	// the `ParseForm` method of the request, parses the
	// form values
	err := r.ParseForm()

	// In case of any error, we respond with an error to the user
	if err != nil {
		fmt.Println(fmt.Errorf("Error: %v", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Get the information about the calc from the form info
	calc.First_number, _ = strconv.ParseFloat(r.Form.Get("first_number"), 64)
	calc.Second_number, _ = strconv.ParseFloat(r.Form.Get("second_number"), 64)
	calc.Result = calc.First_number + calc.Second_number

	// Append our existing list of calcs with a new entry
	calcHistory = append(calcHistory, calc)

	//Finally, we redirect the user to the original HTMl page
	// (located at `/assets/`), using the http libraries `Redirect` method
	http.Redirect(w, r, "/", http.StatusFound)
}
