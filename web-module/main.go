package main

import (
	"fmt"
	"net/http"
	"github.com/gorilla/mux"
	"os"
)

// The new router function creates the router and
// returns it to us. We can now use this function
// to instantiate and test the router outside of the main function
func newRouter() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/hello", handler).Methods("GET")
	r.HandleFunc("/calc", getCalcHistoryHandler).Methods("GET")
	r.HandleFunc("/calc", executeCalcHandler).Methods("POST")
	// Declare the static file directory and point it to the
	// directory we just made
	staticFileDirectory := http.Dir("./assets/")
	// Declare the handler, that routes requests to their respective filename.
	staticFileHandler := http.FileServer(staticFileDirectory)
	// The "PathPrefix" method acts as a matcher, and matches all routes starting
	// with "/", instead of the absolute route itself
	r.PathPrefix("/").Handler(staticFileHandler).Methods("GET")
	// These lines are added inside the newRouter() function before returning r
	return r
}

func main() {
	// The router is now formed by calling the `newRouter` constructor function
	// that we defined above. The rest of the code stays the same
	r := newRouter()
	http.ListenAndServe(":" + os.Getenv("APP_PORT"), r)
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello World!")
}
