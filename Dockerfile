FROM golang:1.20-alpine as builder
WORKDIR /app

COPY web-module .
RUN go get web-module &&\
    CGO_ENABLED=0 GOOS=linux go build -o web-calculator &&\
    ldd web-calculator > /app/ldd-stats; exit 0

ENV APP_PORT 8080
EXPOSE 8080/tcp
ENTRYPOINT ["/app/web-calculator"]

# build go app with local module
FROM golang:1.20-alpine
WORKDIR /app

COPY --from=builder /app/ .
RUN go mod edit -replace=github.com/gorilla/mux@v1.8.0=../mux &&\
    CGO_ENABLED=0 GOOS=linux go build -o web-calculator &&\
    ldd web-calculator > /app/ldd-stats; exit 0

ENV APP_PORT 8080
EXPOSE 8080/tcp
ENTRYPOINT ["/app/web-calculator"]

# build dynamically linked go app
FROM golang:1.20-alpine
WORKDIR /app

RUN apk --no-cache add build-base

COPY --from=builder /app/ .
RUN go get web-module &&\
    CGO_ENABLED=1 GOOS=linux go build -o web-calculator &&\
    ldd web-calculator > /app/ldd-stats; exit 0

ENV APP_PORT 8080
EXPOSE 8080/tcp
ENTRYPOINT ["/app/web-calculator"]