# OPPO-creative-work-CI



## Getting started

Clone repo
```
git clone https://gitlab.com/qreodium/oppo-creative-work-ci
```

Initiliaze submodules
```
git submodule init
```
To build docker images use:
```
docker build .
```
After that you can run the contaniner with app

## Deploy to kubernetes
To deploy the app to kubernetes use [Flux2](https://fluxcd.io/flux/).\
oppo-calc-source.yaml:
```
---
apiVersion: source.toolkit.fluxcd.io/v1beta2
kind: GitRepository
metadata:
  name: oppo-calc
  namespace: flux-system
spec:
  interval: 1m0s
  ref:
    branch: main
  url: https://gitlab.com/qreodium/oppo-creative-work-ci
```
oppo-calc-kustomization.yaml:
```
---
apiVersion: kustomize.toolkit.fluxcd.io/v1beta2
kind: Kustomization
metadata:
  name: oppo-calc
  namespace: flux-system
spec:
  healthChecks:
  - kind: Deployment
    name: oppo-calc
    namespace: oppo-calc
  interval: 10m0s
  path: ./kustomize
  prune: true
  sourceRef:
    kind: GitRepository
    name: oppo-calc
  targetNamespace: oppo-calc
  timeout: 2m0s
```